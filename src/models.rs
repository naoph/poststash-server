use chrono::{DateTime, Utc};

use crate::schema::*;

#[derive(Debug, Queryable)]
pub struct DbDocument {
    pub id: i64,
    pub filename: String,
    pub inserted: DateTime<Utc>,
    pub comment: Option<String>,
    pub thumbnail: Option<String>,
    pub tags: Vec<String>,
}

#[derive(Debug, Insertable)]
#[diesel(table_name = documents)]
pub struct InsDocument {
    pub filename: String,
    pub inserted: DateTime<Utc>,
    pub comment: Option<String>,
    pub thumbnail: Option<String>,
    pub tags: Vec<String>,
}
