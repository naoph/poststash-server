table! {
    documents (id) {
        id -> Int8,
        filename -> Text,
        inserted -> Timestamptz,
        comment -> Nullable<Text>,
        thumbnail -> Nullable<Text>,
        tags -> Array<Text>,
    }
}
