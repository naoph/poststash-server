use std::sync::Arc;
use std::collections::HashMap;

use tokio::sync::Mutex;
use url::Url;

use poststash::api::response::DocumentCreateStatusResp;

pub mod dispatch;
pub mod install;

pub struct DocSrcUrlMap {
    map: Arc<Mutex<HashMap<Url, DocumentCreateStatusResp>>>,
}

impl DocSrcUrlMap {
    /// Create an empty map
    pub fn new() -> DocSrcUrlMap {
        DocSrcUrlMap {
            map: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    /// Determine whether known-extractable URL should be extracted, preemptively marking
    /// extraction as ongoing if so
    pub async fn try_set_ongoing(&self, url: &Url) -> bool {
        let mut map = self.map.lock().await;
        let should_extract = match map.get(url) {
            // Extraction hasn't been attempted before; try
            None => true,

            // Extraction has failed previously; try again
            Some(&DocumentCreateStatusResp::Failure { message: _ }) => true,

            // Extraction is ongoing; ignore
            Some(&DocumentCreateStatusResp::Ongoing) => false,

            // Extraction has already succeeded; ignore
            Some(&DocumentCreateStatusResp::Success { document_id: _ }) => false,
        };
        if should_extract {
            map.insert(url.clone(), DocumentCreateStatusResp::Ongoing);
        }
        should_extract
    }

    /// Mark extraction as failed with provided error message
    pub async fn set_failure(&self, url: &Url, message: String) {
        self.map.lock().await.insert(url.clone(), DocumentCreateStatusResp::Failure { message });
    }

    /// Mark extraction as finished with provided document ID
    pub async fn set_finished(&self, url: &Url, document_id: i64) {
        self.map.lock().await.insert(url.clone(), DocumentCreateStatusResp::Success { document_id });
    }

    /// Get the status of a given URL
    pub async fn get_status(&self, url: &Url) -> Option<DocumentCreateStatusResp> {
        self.map.lock().await.get(url).map(|s| s.clone())
    }
}
