use std::{path::PathBuf, ffi::OsString};

use bundledoc::{Document, EmbeddedBinary};
use diesel::prelude::*;
use regex::Regex;
use snafu::prelude::*;
use url::Url;

use crate::{models, schema, POOL};
use crate::{DOCSRCURLMAP, INSTALLER};

/// Stores extractor rules and uses them to assign URLs to extractors
pub struct ExtractorDispatch {
    extractor_dir: PathBuf,
    rules: Vec<ExtractorRule>,
}

impl ExtractorDispatch {
    /// Process extractor rules and initialize
    pub fn init(
        dir: impl Into<PathBuf>,
        rules: Vec<(String, String)>,
    ) -> Result<ExtractorDispatch, ExtractorDispatchInitError> {
        use ExtractorDispatchInitError::*;

        // Ensure the directory is absolute & a directory
        let extractor_dir: PathBuf = dir.into();
        if !(extractor_dir.is_absolute() && extractor_dir.is_dir()) {
            return Err(NotAbsoluteDir { extractor_dir });
        };

        // Get contents of extractor directory for verification
        let dir_listing = std::fs::read_dir(&extractor_dir)
            .or(Err(CantReadDir { extractor_dir: extractor_dir.clone() }))?
            .into_iter()
            .filter_map(|i| i.ok())
            .map(|i| i.file_name())
            .collect::<Vec<_>>();

        // Process each rule's regex and extractor
        let mut processed_rules = Vec::new();
        for (regex, extractor) in rules {
            if !dir_listing.contains(&OsString::from(&extractor)) {
                return Err(MissingExtractor { extractor });
            }
            let processed_regex = Regex::new(&format!(r"^{}$", regex))
                .context(InvalidRegexSnafu { extractor: extractor.clone() })?;
            processed_rules.push(ExtractorRule::new(processed_regex, extractor));
        }

        Ok(ExtractorDispatch {
            extractor_dir,
            rules: processed_rules,
        })
    }

    /// Try to assign a URL to an extractor, spawning said extractor if successful
    pub async fn dispatch(&self, url: Url) -> Result<String, ExtractorDispatchDispatchError> {
        use ExtractorDispatchDispatchError::*;

        // Extermine extractor that should be used
        let extractor = self.determine_extractor(&url).await
            .ok_or(NoExtractor)?;

        // Try marking for extraction
        if !DOCSRCURLMAP.try_set_ongoing(&url).await {
            return Err(DuplicateUrl);
        }

        // Spawn extractor
        tokio::spawn(extract(self.extractor_dir.clone(), extractor.clone(), url));

        Ok(extractor)
    }

    /// Find matching extractor rule
    async fn determine_extractor(&self, url: &Url) -> Option<String> {
        for rule in self.rules.iter() {
            if rule.regex.is_match(url.as_str()) {
                return Some(rule.extractor.clone())
            }
        }
        None
    }
}

/// Run a predetermined extractor for a given URL
async fn extract(extractor_dir: PathBuf, extractor: String, url: Url) {
    // Get the full path of the extractor
    let full_extractor = extractor_dir.join(&extractor);

    // Run the extractor and get its output/status
    let output = tokio::process::Command::new(&full_extractor)
        .arg(url.to_string())
        .output()
        .await;

    // Ensure it ran
    let output = match &output {
        Ok(o) => o,
        Err(e) => {
            error!("Failed to start extractor {:?} on url {url}: {e}", &extractor);
            DOCSRCURLMAP.set_failure(&url, e.to_string()).await;
            return;
        }
    };

    // Ensure it completed successfully
    if !output.status.success() {
        let error_msg = match std::str::from_utf8(&output.stderr) {
            Ok(msg) => msg.to_string(),
            Err(_) => format!("{:?}", output.stderr),
        };
        error!("Extractor {extractor} exited nonzero on url {url}: {error_msg}");
        DOCSRCURLMAP.set_failure(&url, error_msg).await;
        return;
    }

    // Send bundledoc off to installer
    let bundle: Document<EmbeddedBinary> = match Document::from_bin(&output.stdout).await {
        Ok(b) => b,
        Err(e) => {
            error!("Received invalid bundle from extractor {extractor}: {e}");
            DOCSRCURLMAP.set_failure(&url, e.to_string()).await;
            return;
        },
    };
    let filename = match INSTALLER.install_document(bundle).await {
        Ok(f) => f,
        Err(e) => {
            error!("Unable to install document: {e}");
            DOCSRCURLMAP.set_failure(&url, e.to_string()).await;
            return;
        },
    };

    // Insert into database
    let ins_doc: models::InsDocument = models::InsDocument {
        filename,
        inserted: chrono::Utc::now(),
        comment: None,
        thumbnail: None,
        tags: Vec::new(),
    };
    let mut conn = match POOL.get() {
        Ok(c) => c,
        Err(e) => {
            error!("Error connecting to database: {e}");
            DOCSRCURLMAP.set_failure(&url, "Error connecting to database".to_string()).await;
            return;
        },
    };
    let result: Result<models::DbDocument, _> = diesel::insert_into(schema::documents::table)
        .values(&ins_doc)
        .get_result(&mut conn);
    match result {
        Ok(d) => {
            DOCSRCURLMAP.set_finished(&url, d.id).await;
        },
        Err(e) => {
            error!("Error inserting document to database: {e}");
            DOCSRCURLMAP.set_failure(&url, "Error inserting to database".to_string()).await;
            return;
        },
    }
}

/// Pairing between an extractor and a regex matching URLs the extractor should be used for
struct ExtractorRule {
    regex: Regex,
    extractor: String,
}

impl ExtractorRule {
    pub fn new(regex: Regex, extractor: String) -> ExtractorRule {
        ExtractorRule {
            regex,
            extractor,
        }
    }
}

#[derive(Debug, Snafu)]
pub enum ExtractorDispatchInitError {
    #[snafu(display("Is not an absolute directory: {:?}", extractor_dir))]
    NotAbsoluteDir { extractor_dir: PathBuf },

    #[snafu(display("Can't read directory: {:?}", extractor_dir))]
    CantReadDir { extractor_dir: PathBuf },

    #[snafu(display("Missing extractor: {:?}", extractor))]
    MissingExtractor { extractor: String },

    #[snafu(display("Invalid regex for extractor {}: {:?}", extractor, source))]
    InvalidRegex { extractor: String, source: regex::Error },
}

#[derive(Debug, Snafu)]
pub enum ExtractorDispatchDispatchError {
    #[snafu(display("No extractor available for this URL"))]
    NoExtractor,

    #[snafu(display("URL has already been extracted or is currently being extracted"))]
    DuplicateUrl,
}
