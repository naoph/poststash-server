use std::{io::{BufReader, Read, Write}, path::PathBuf, ffi::OsString};

use async_trait::async_trait;
use sha2::{Digest, Sha256};
use snafu::prelude::*;

use bundledoc::{BinaryInstaller, EmbeddedBinary, format::*, Document};

/// Stores media path info & installs things to it
pub struct Installer {
    media_dir: PathBuf,
    shard_levels: u8,
}

impl Installer {
    pub fn init(media_dir: PathBuf, shard_levels: u8) -> Installer {
        Installer { media_dir, shard_levels }
    }

    /// Convert document with embedded media to document with detached media, installing all
    /// detached media as well as the resulting document
    pub async fn install_document(&self, document: Document<EmbeddedBinary>) -> Result<String, DocumentInstallationError> {
        let detached_document = document.detach(self).await.context(CantInstallFileSnafu)?;
        let dd_bin = detached_document.to_bin().await.context(CantSerializeDocumentSnafu)?;
        self.install_file(&dd_bin[..], "document".to_string()).await
            .context(CantInstallDocumentSnafu)
    }

    /// Install arbitrary binary to a location derived from its hash
    pub async fn install_file(&self, data: &[u8], extension: String) -> Result<String, FileInstallationError> {
        // Ensure media directory exists
        if !self.media_dir.is_dir() {
            tokio::fs::create_dir_all(&self.media_dir).await.context(BadMediaDirectorySnafu)?;
        }

        // Write to temp file & hash
        let mut br = BufReader::new(data);
        let mut buf: [u8; 4096] = [0; 4096];
        let mut tmp = tempfile::NamedTempFile::new_in(&self.media_dir)
            .context(CantCreateTempFileSnafu)?;
        let mut hasher = Sha256::new();
        while let Ok(qty) = br.read(&mut buf) {
            if qty == 0 {
                break;
            }
            tmp.write(&buf[..qty]).context(CantCreateTempFileSnafu)?;
            hasher.update(&buf[..qty]);
        }

        // Calculate final hash
        let hash_bytes = hasher.finalize().to_vec();
        let hash = hex::encode(&hash_bytes);

        // Determine ultimate path & create its parent directories if needed
        let mut shard_dir = PathBuf::from(&self.media_dir);
        let shards = &hash_bytes[..self.shard_levels as usize];
        for shard in shards {
            let hex_shard = OsString::from(hex::encode([*shard]));
            shard_dir.push(hex_shard);
        }
        tokio::fs::create_dir_all(&shard_dir).await.context(CantCreateShardDirectorySnafu)?;
        let filename = format!("{hash}.{extension}");
        let final_dest = shard_dir.join(&filename);

        // Move temp file to ultimate path
        tokio::fs::rename(tmp.path(), final_dest).await.context(CantMoveTempFileSnafu)?;

        info!("Installed: {filename}");
        Ok(filename)
    }
}

#[async_trait]
impl BinaryInstaller for Installer {
    type DetErr = FileInstallationError;

    /// Install media elements
    async fn install(&self, bin: EmbeddedBinary, format: &MediaFormat) -> Result<String, Self::DetErr> {
        self.install_file(&bin.bin[..], format.extension()).await
    }
}

#[derive(Debug, Snafu)]
pub enum FileInstallationError {
    #[snafu(display("Media directory does not exist and could not be created: {source}"))]
    BadMediaDirectory { source: std::io::Error },

    #[snafu(display("Couldn't create temp file: {source}"))]
    CantCreateTempFile { source: std::io::Error },

    #[snafu(display("Couldn't write chunk to temp file: {source}"))]
    CantWriteTempFile { source: std::io::Error },

    #[snafu(display("Couldn't create shard directory: {source}"))]
    CantCreateShardDirectory { source: std::io::Error },

    #[snafu(display("Couldn't move temp file to its ultimate location: {source}"))]
    CantMoveTempFile { source: std::io::Error },
}

#[derive(Debug, Snafu)]
pub enum DocumentInstallationError {
    #[snafu(display("Error installing file: {source}"))]
    CantInstallFile { source: FileInstallationError },

    #[snafu(display("Error serializing document: {source}"))]
    CantSerializeDocument { source: rmp_serde::encode::Error },

    #[snafu(display("Error installing successfully detached document: {source}"))]
    CantInstallDocument { source: FileInstallationError },
}
