use std::path::PathBuf;

use serde::Deserialize;
use snafu::prelude::*;
use url::Url;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub server_listen: ([u8; 4], u16),
    pub database_url: Url,
    pub media_dir: PathBuf,
    pub media_shards: u8,
    pub extractor_dir: PathBuf,
    pub extractor_rules: Vec<(String, String)>,
}

impl Config {
    pub fn load(path: impl Into<PathBuf>) -> Result<Config, ConfigLoadError> {
        use ConfigLoadError::*;

        let path: PathBuf = path.into();

        // Ensure path is absolute and a regular file
        if !path.is_absolute() {
            return Err(NotAbsolute { path });
        }
        if !path.is_file() {
            return Err(NotFile { path });
        }

        // Read contents of file
        let config_data = std::fs::read_to_string(path)
            .context(CantReadFileSnafu)?;

        // Parse
        let config: Config = ron::from_str(&config_data)
            .context(InvalidConfigSnafu)?;

        Ok(config)
    }
}

#[derive(Debug, Snafu)]
pub enum ConfigLoadError {
    #[snafu(display("Is not an absolute path: {:?}", path))]
    NotAbsolute { path: PathBuf },

    #[snafu(display("Is not a regular file: {:?}", path))]
    NotFile { path: PathBuf },

    #[snafu(display("Couldn't read file: {:?}", source))]
    CantReadFile { source: std::io::Error },

    #[snafu(display("Invalid config file: {}", source))]
    InvalidConfig { source: ron::error::SpannedError },
}
