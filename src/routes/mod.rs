mod endpoints;

use warp::{filters::BoxedFilter, Filter, Reply};

use crate::CONFIG;

pub async fn routes() -> BoxedFilter<(impl Reply,)> {
    // Media
    let media_files = warp::get()
        .and(warp::path("media"))
        .and(warp::fs::dir(CONFIG.media_dir.to_owned()));

    // POST /document/create
    let post_document_create = warp::post()
        .and(warp::path!("document" / "create"))
        .and(warp::body::json())
        .and_then(endpoints::document_create);

    // GET /document/create/status/<url>
    let get_document_create_status = warp::get()
        .and(warp::path!("document" / "create" / "status" / String))
        .and_then(endpoints::document_create_status);

    // Combine all routes
    let routes = warp::any()
        .and(media_files
             .or(post_document_create)
             .or(get_document_create_status));

    routes.boxed()
}
