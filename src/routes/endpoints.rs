use http::StatusCode;
use serde::Serialize;
use url::Url;
use warp::{Rejection, reply};

use poststash::api::request::*;
use poststash::api::response::*;

use crate::DOCSRCURLMAP;
use crate::state::dispatch::ExtractorDispatchDispatchError;
use crate::EXTRACTORDISPATCH;

type StatusJson = warp::reply::WithStatus<warp::reply::Json>;

/// Create a reply from a JSON-able type and an HTTP status code
async fn json<T: Serialize>(payload: T, status: StatusCode) -> Result<StatusJson, Rejection> {
    Ok(reply::with_status(reply::json(&payload), status))
}

/// Begin extracting a URL, adding it to the database if successful
pub async fn document_create(req: DocumentCreateReq) -> Result<StatusJson, Rejection> {
    use ExtractorDispatchDispatchError::*;

    match EXTRACTORDISPATCH.dispatch(req.url.clone()).await {
        Ok(e) => json(DocumentCreateResp::Started { extractor: e }, StatusCode::OK).await,
        Err(NoExtractor) => json(DocumentCreateResp::UnhandledUrl, StatusCode::NOT_IMPLEMENTED).await,
        Err(DuplicateUrl) => json(DocumentCreateResp::Duplicate, StatusCode::CONFLICT).await,
    }
}

pub async fn document_create_status(encurl: String) -> Result<StatusJson, Rejection> {
    // Decode the URL
    let url = match urlencoding::decode(&encurl) {
        Ok(u) => u.to_string(),
        Err(_) => {
            return json(
                DocumentCreateStatusRespWrap::InvalidEncodedUrl,
                StatusCode::BAD_REQUEST,
            ).await
        },
    };

    // Parse the URL
    let url = match Url::parse(&url) {
        Ok(u) => u,
        Err(_) => {
            return json(
                DocumentCreateStatusRespWrap::InvalidUrl,
                StatusCode::BAD_REQUEST,
            ).await
        },
    };

    // Lookup
    match DOCSRCURLMAP.get_status(&url).await {
        Some(s) => json(DocumentCreateStatusRespWrap::Exists{ status: s }, StatusCode::OK).await,
        None => json(DocumentCreateStatusRespWrap::UnknownUrl, StatusCode::NOT_FOUND).await,
    }
}
