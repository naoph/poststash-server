#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;

mod config;
mod models;
mod routes;
mod schema;
mod state;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

type PgPool = r2d2::Pool<ConnectionManager<PgConnection>>;

lazy_static! {
    // Map recording the extraction status of URLs
    pub static ref DOCSRCURLMAP: state::DocSrcUrlMap = {
        state::DocSrcUrlMap::new()
    };

    // Config options from disc
    pub static ref CONFIG: config::Config = {
        // Get config path, either custom from $POSTSTASH_SERVER_CONF or default from $HOME
        let conf_path = match std::env::var("POSTSTASH_SERVER_CONF") {
            Ok(p) => p,
            Err(_) => {
                let path = dirs::home_dir()
                    .expect("Couldn't find config. Try setting $POSTSTASH_SERVER_CONF or $HOME");

                path.join(".config")
                    .join("poststash")
                    .join("server.conf")
                    .to_str()
                    .expect("Config path {} is not legal")
                    .to_string()
            }
        };

        match config::Config::load(&conf_path) {
            Ok(c) => c,
            Err(e) => panic!("Error loading config `{}`: {}", &conf_path, e),
        }
    };

    // Assigns URLs to their respective extractors
    pub static ref EXTRACTORDISPATCH: state::dispatch::ExtractorDispatch = {
        let extractor_dir = CONFIG.extractor_dir.clone();
        let extractor_rules = CONFIG.extractor_rules.clone();
        match state::dispatch::ExtractorDispatch::init(extractor_dir, extractor_rules) {
            Ok(e) => e,
            Err(e) => panic!("Error loading extractors: {}", e),
        }
    };

    // Installs documents and their associated media to disc
    pub static ref INSTALLER: state::install::Installer = {
        state::install::Installer::init(CONFIG.media_dir.clone(), CONFIG.media_shards)
    };

    // Database connection pool
    pub static ref POOL: PgPool = {
        let manager = ConnectionManager::<PgConnection>::new(CONFIG.database_url.to_string());
        let pool = r2d2::Pool::builder().build(manager);
        match pool {
            Ok(p) => p,
            Err(e) => {
                error!("Failed to establish database connection: {e}");
                panic!();
            }
        }
    };
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    // Make sure some fallible lazies are run immediately
    lazy_static::initialize(&EXTRACTORDISPATCH);
    lazy_static::initialize(&POOL);

    // Start server
    warp::serve(routes::routes().await).run(CONFIG.server_listen).await;
}
