CREATE TABLE documents (
    id BIGSERIAL PRIMARY KEY,
    filename text UNIQUE NOT NULL,
    inserted timestamp with time zone NOT NULL,
    comment text,
    thumbnail text,
    tags text[] NOT NULL
);
